# Wapahhh

## Thank you for playing my game!

Check out the [Terms And Conditions](https://gitlab.com/powers.bart/wapahhh/blob/master/terms-and-conditions)
and the [Privacy Policy](https://gitlab.com/powers.bart/wapahhh/blob/master/privacy-policy)

Post any comments or feed back on here as an issue, or message me direct on instagram [@wapahh](https://www.instagram.com/wapahh/)
